# Проект Pyatilistnik Ink.

Данный проект является составляющей IT блога http://pyatilistnik.org/ созданного для компьютерной помощи по многим вопросам, связанным с настройкой серверов, операционных систем Windows, Linux.
Настройка различных гипервизоров VMware ESXI (http://pyatilistnik.org/vmware/), Microsoft Hyper-V (http://pyatilistnik.org/hyper-v/) и многое другое.